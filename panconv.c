/*
 * © Copyright 2017 The Panfrost Community
 *
 * This program is free software and is provided to you under the terms of the
 * GNU General Public License version 2 as published by the Free Software
 * Foundation, and any use by you of this program is subject to the terms
 * of such GNU licence.
 *
 * A copy of the licence is included with the program, and can also be obtained
 * from Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdarg.h>
#include <string.h>
#include <errno.h>
#include <time.h>
#include <ctype.h>
#include <sys/ioctl.h>
#include <dlfcn.h>

/**
 * Grab the location of a symbol from the system's libc instead of our
 * preloaded one
 */
void *
__rd_dlsym_helper(const char *name)
{
	static void *libc_dl;
	void *func;

	if (!libc_dl)
		libc_dl = dlopen("libc.so", RTLD_LAZY);
	if (!libc_dl)
		libc_dl = dlopen("libc.so.6", RTLD_LAZY);
	if (!libc_dl) {
		fprintf(stderr, "Failed to dlopen libc: %s\n", dlerror());
		exit(-1);
	}

	func = dlsym(libc_dl, name);
	if (!func) {
		fprintf(stderr, "Failed to find %s: %s\n", name, dlerror());
		exit(-1);
	}

	return func;
}

#define PROLOG(func) \
	static typeof(func) *orig_##func = NULL; \
	if (!orig_##func) \
		orig_##func = __rd_dlsym_helper(#func);

typedef int (*ioctl_handler)(int fd, unsigned long req, ...);

struct panconv_ioctl_handlers {
	ioctl_handler handlers[1 << _IOC_TYPEBITS][1 << _IOC_NRBITS];
};

static const struct panconv_ioctl_handlers drm_to_kbase;
static const struct panconv_ioctl_handlers kbase_to_drm;

struct panconv_ctx {
	const struct panconv_ioctl_handlers *handlers;
};

#define MAX_FDS UINT16_MAX

struct panconv_ctx *contexts[MAX_FDS];

static int ctx_to_fd(struct panconv_ctx *ctx)
{
	return ctx - contexts[0];
}


static const char *from, *to;

static void __attribute__((constructor))
panconv_constructor(void)
{
	from = getenv("PANCONV_FROM");
	to = getenv("PANCONV_TO");
}

static void
panconv_free_ctx(int fd)
{
	if (fd < 0 || fd >= MAX_FDS || !contexts[fd])
		return;

	free(contexts[fd]);
}

static struct panconv_ctx *
panconv_create_ctx(int fd, const struct panconv_ioctl_handlers *handlers)
{
	struct panconv_ctx *ctx = calloc(1, sizeof(*ctx));

	assert(fd >= 0 && fd < MAX_FDS);
	assert(ctx);
	ctx->handlers = handlers;
	contexts[fd] = ctx;
	return ctx;
}

int
open(const char *path, int flags, ...)
{
	int ret;

	PROLOG(open);
	va_list args;
	va_start(args, flags);

	if (!from || !to || strcmp(from, path)) {
		ret = orig_open(path, flags, args);
	} else {
		ret = orig_open(to, flags, args);
		if (ret >= 0) {
			struct panconv_ctx *ctx =
				panconv_create_ctx(ret,
						   !strncmp(from, "/dev/mali", 9) ?
						   &kbase_to_drm : &drm_to_kbase);
		}
	}

	va_end(args);
	return ret;
}

int
close(int fd)
{
	PROLOG(close);
	panconv_free_ctx(fd);
	return orig_close(fd);
}

int
ioctl(int fd, unsigned long req, ...)
{
	int ret;
	PROLOG(ioctl);

	va_list args;
	va_start(args, req);

	if (fd < 0 || fd >= MAX_FDS || !contexts[fd]) {
		ret = orig_ioctl(fd, req, args);
	} else {
		ioctl_handler handler = contexts[fd]->handlers->handlers[_IOC_TYPE(req)][_IOC_NR(req)];

		if (!handler) {
			fprintf(stderr, "Unhandled ioctl (type=%02lx,nr=%02lx)", _IOC_TYPE(req), _IOC_NR(req));
			exit(-1);
		}

		ret = handler(fd, req, args);
	}

	va_end(args);
	return ret;
}
